const fs = require('fs');
const key = fs.readFileSync('server.key');
const cert = fs.readFileSync('server.crt');

const app = require('express')();
const https = require('https').Server({key, cert}, app);
const io = require('socket.io')(https);

io.on('connection', socket => {
    socket.on('signal', data => {
        socket.broadcast.emit('signal', data);
    });

    socket.on('initial', data => {
        socket.broadcast.emit('initial', data);
    })
});

https.listen(3000, function(){
    console.log('listening on *:3000');
});
